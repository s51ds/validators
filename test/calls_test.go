package test

import (
	"bufio"
	"fmt"
	"gitlab.com/s51ds/validators/validate"
	"log"
	"os"
	"testing"
)

func TestCallSignsFromMasterSCP(t *testing.T) {
	f, err := os.Open("master.scp")
	if err != nil {
		log.Fatal(err)
	}
	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			fmt.Println(err.Error())
		}
	}(f)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		call := scanner.Text()
		if !validate.CallSign(call) {
			fmt.Println(call)
		}

	}

}
